# [2.4.0](https://gitlab.com/to-be-continuous/terraform/compare/2.3.2...2.4.0) (2022-01-10)


### Features

* Vault variant + non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([4f1265a](https://gitlab.com/to-be-continuous/terraform/commit/4f1265a39edd87fcac848778d2f687661e527421))

## [2.3.2](https://gitlab.com/to-be-continuous/terraform/compare/2.3.1...2.3.2) (2021-12-21)


### Bug Fixes

* **ci:** only launch production plan when merge request targets the prod branch ([898275e](https://gitlab.com/to-be-continuous/terraform/commit/898275e7fc5298621a34fc7ca2349ab7898befe2))

## [2.3.1](https://gitlab.com/to-be-continuous/terraform/compare/2.3.0...2.3.1) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([f552ee2](https://gitlab.com/to-be-continuous/terraform/commit/f552ee24d31d5d38cb68717008fe24bee0f374b7))

# [2.3.0](https://gitlab.com/to-be-continuous/terraform/compare/2.2.4...2.3.0) (2021-10-18)


### Features

* add plugin for cloud providers ([370c141](https://gitlab.com/to-be-continuous/terraform/commit/370c141bfeaab4cfa6d3bf7b179a2a5de2e2e8ad))

## [2.2.4](https://gitlab.com/to-be-continuous/terraform/compare/2.2.3...2.2.4) (2021-10-18)


### Bug Fixes

* revert test jobs on change only ([53378e4](https://gitlab.com/to-be-continuous/terraform/commit/53378e4e3b18f9d914e3c621d970a18b2a09edfb))

## [2.2.3](https://gitlab.com/to-be-continuous/terraform/compare/2.2.2...2.2.3) (2021-10-15)


### Bug Fixes

* update tflint image ([78014a7](https://gitlab.com/to-be-continuous/terraform/commit/78014a766ffc6543d6d2c71efa67b92e8f590eaf))

## [2.2.2](https://gitlab.com/to-be-continuous/terraform/compare/2.2.1...2.2.2) (2021-10-15)


### Bug Fixes

* enable change on subdir ([e80d07d](https://gitlab.com/to-be-continuous/terraform/commit/e80d07d39f14e91954c69568d26178b34aa2c6c3))

## [2.2.1](https://gitlab.com/to-be-continuous/terraform/compare/2.2.0...2.2.1) (2021-10-13)


### Bug Fixes

* only launch tests job on code update ([38b1143](https://gitlab.com/to-be-continuous/terraform/commit/38b114355805491bc03e7f12961775a086485808))

# [2.2.0](https://gitlab.com/to-be-continuous/terraform/compare/2.1.1...2.2.0) (2021-10-13)


### Features

* allow to override Terraform commands using a GitLab reference feature. ([64bed8a](https://gitlab.com/to-be-continuous/terraform/commit/64bed8a7973f133056542a131648b542479456e5))

## [2.1.1](https://gitlab.com/to-be-continuous/terraform/compare/2.1.0...2.1.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([fb622a1](https://gitlab.com/to-be-continuous/terraform/commit/fb622a1a34d5d77d4de4e472e77ebdb50977999d))

# [2.1.0](https://gitlab.com/to-be-continuous/terraform/compare/2.0.0...2.1.0) (2021-09-19)


### Features

* add infracost ([1ffba7f](https://gitlab.com/to-be-continuous/terraform/commit/1ffba7f5eb3679c8c314d6e9fb8a696f31525a09))

## [2.0.0](https://gitlab.com/to-be-continuous/terraform/compare/1.1.2...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([f175aed](https://gitlab.com/to-be-continuous/terraform/commit/f175aed5f75a03802e70e7736c429c132e8ca565))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.1.2](https://gitlab.com/to-be-continuous/terraform/compare/1.1.1...1.1.2) (2021-07-21)

### Bug Fixes

* remove dependencies on tfsec, tflint and checkcov ([f8f1e87](https://gitlab.com/to-be-continuous/terraform/commit/f8f1e871574c8a14ae36002efe2d9a0288476ad0))

## [1.1.1](https://gitlab.com/to-be-continuous/terraform/compare/1.1.0...1.1.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([27ebea7](https://gitlab.com/to-be-continuous/terraform/commit/27ebea71b1f872200ccf3f0d1d7c08f1fb9d8e3d))

## [1.1.0](https://gitlab.com/to-be-continuous/terraform/compare/1.0.0...1.1.0) (2021-06-10)

### Features

* move group ([869658c](https://gitlab.com/to-be-continuous/terraform/commit/869658cd53feb93b984028556114c318f872fc9b))

## 1.0.0 (2021-05-06)

### Features

* initial release ([9421d94](https://gitlab.com/Orange-OpenSource/tbc/terraform/commit/9421d94e6fd8149da03831dcda3723baaed7d745))
